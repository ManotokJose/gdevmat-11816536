void setup()
{
  size(1920, 1080, P3D);
  camera (0,0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f),
    0, 0, 0,
    0, -1, 0);
    background(0);
}

 int frameNumber = 0;

void draw()
{
  float gaussianX = randomGaussian();
  float randomY = random(-height, height);
  float gaussianSize = randomGaussian();
 
  float mean = 0;
  float standardDeviation = 120;
  
  float x = (standardDeviation * gaussianX) + mean;
  float size = (gaussianSize * standardDeviation);
  
  noStroke();
  fill(random(255), random(255), random(255), random(255));
  circle(x, randomY, size);
  
  if(frameNumber > 1000)
  {
    clear();
    frameNumber = 0;
  }
  
  frameNumber = frameNumber + 1;
}
