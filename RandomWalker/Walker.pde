class Walker
{
  float xPos;
  float yPos;
  
  Walker() 
  {
  };
  
  Walker(float x, float y)
  {
    xPos = x;
    yPos = y;
  }
  
  void render()
  {
   noStroke();
   fill(colour);
   circle(xPos, yPos, 30); 
  }
  
  color colour;
  
  void randomWalk()
  {
    int direction = floor(random(8));
    
    if(direction == 0)
    {
      yPos += 10;
      colour = color(255, 0, 0);
    }
    else if(direction == 1)
    {
      yPos -= 10;
      colour = color(255, 255, 0);
    }
    else if(direction == 2)
    {
      xPos += 10;
      colour = color(0, 255, 0);
    }
    else if(direction == 3)
    {
      xPos -= 10;
      colour = color(0, 255, 255);
    }
    else if(direction == 4)
    {
      yPos -= 10;
      xPos -= 10;
      colour = color(0, 0, 255);
    }
     else if(direction == 5)
    {
      yPos += 10;
      xPos -= 10;
      colour = color(255, 0, 255);
    }
     else if(direction == 6)
    {
      yPos -= 10;
      xPos += 10;
      colour = color(50, 50, 50);
    }
     else if(direction == 7)
    {
      yPos += 10;
      xPos += 10;
      colour = color(200, 69, 5);
    }
  }
}
