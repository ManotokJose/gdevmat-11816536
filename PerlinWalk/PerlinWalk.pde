void setup()
{
  size(1600, 900, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
  background(0);
  
}

float xPos = 0;
float yPos = 0;
float xNoise = 0;
float yNoise = 1;
float cSize = 100;

float r_t = 100;
float g_t = 200;
float b_t = 150;

void draw()
{
  
  float circleSize = map(noise(cSize), 0, 1, 0, 100);
  
  xPos = map(noise(xNoise), 0, 1, 0, width);
  yPos = map(noise(yNoise), 0, 1, 0, height);
  
  xNoise += .005;
  yNoise += .005;
  cSize += 1;
  
  noStroke();
  fill(map(noise(r_t), 0, 1, 0, 500),
      map(noise(g_t), 0, 1, 0, 500), 
      map(noise(b_t), 0, 1, 0, 500));
      
  r_t += 0.1;
  g_t += 0.1;
  b_t += 0.1;
  
  
  circle(xPos, yPos, circleSize);
}
